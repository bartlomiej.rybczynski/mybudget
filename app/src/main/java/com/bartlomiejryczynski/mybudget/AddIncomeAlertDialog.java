package com.bartlomiejryczynski.mybudget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddIncomeAlertDialog extends DialogFragment {
    private DB mydb;
    public String nazwa ;

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        // Use the Builder class for convenient dialog construction
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setMessage("Whatever")
//
////                R.string.dialog_fire_missiles)
//                .setPositiveButton(/*R.string.fire*/ "fire", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // FIRE ZE MISSILES!
//                    }
//                })
//                .setNegativeButton(/*.string.cancel*/"cancel", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // User cancelled the dialog
//                    }
//                });
//        // Create the AlertDialog object and return it
//        return builder.create();

//    }


    private EditText mEditText;
    LastOperation lo = new LastOperation();

    public AddIncomeAlertDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        DB mydb = new DB()
//        getActivity();
        View view = inflater.inflate(R.layout.add_income_dialog, container);
        mEditText = (EditText) view.findViewById(R.id.amountEditText);


        Button button = view.findViewById(R.id.okButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveValues();
            }
        });


        Spinner spinner = (Spinner) view.findViewById(R.id.catPicker);

        //Sample String ArrayList
        ArrayList<String> arrayList1 = new ArrayList<String>();

        Cursor rs = mydb.getCategoryNames();

        rs.moveToFirst();
        for ( int i=0 ; i< rs.getCount() ; i++){
            arrayList1.add(rs.getString(rs.getColumnIndex("category")));
        }
        arrayList1.add("Dodaj Kategorie");

        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,arrayList1);
        spinner.setAdapter(adp);
        spinner.setVisibility(View.VISIBLE);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long arg3)
            {
                String selection = parent.getItemAtPosition(position).toString();
                switch ( selection){
                    case "Dodaj Kategorie":
                        Intent i = new Intent(getContext(), IncomeActivity.class); //TODO add class to create new category
                        startActivity(i);

                        dismiss();
                        break;

                    default:
                        lo.setCategory(selection);

                }

//                Log.d("czuwaj",city);
            }

            public void onNothingSelected(AdapterView<?> arg0)
            {
                // TODO Auto-generated method stub
            }
        });
        getDialog().setTitle("Hello");


        return view;
    }

    public void saveValues(){
//        LastOperation lo = new LastOperation();
        lo.setAmount(Double.parseDouble(mEditText.getText().toString()));

        mydb.insertOperation(lo.getCategory(), lo.getAmount(),"ING","Bartek");
//        Log.d("poczuj", "DUPA JAS Z dialogu:" + lo.getAmount());
        dismiss();
        ((MainActivity) getActivity()).showLastOperations();


    }
    public void setMydb(DB mydb)
    {
        this.mydb = mydb;
    }


}