package com.bartlomiejryczynski.mybudget;


import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private DB mydb ;
    private ListView list ;
    private ArrayAdapter<String> adapter ;
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mydb = new DB(this);


        showLastOperations();
    }

//    protected void insertData(String category, double amount, String accountid, String useraccount){
//    mydb.insertOperation(category, amount, accountid, useraccount);
//
//    }


    public void addExpense(View v){
        AddIncomeAlertDialog newFra   = new AddIncomeAlertDialog();
        newFra.setMydb(mydb);
        newFra.show(getSupportFragmentManager(), "Dialog");
    }

    public void addIncome(View v){
            Intent i = new Intent(getApplicationContext(), IncomeActivity.class);
            startActivity(i);


//        insertData("Kategoria", 123.45, "MILLENIUM", "Bartek");

    }
    public void showLastOperations(){

        String carsp[] = {"cos1","cos2"};
        list = (ListView) findViewById(R.id.listView1);
        ArrayList<String> list1 = new ArrayList<String>();
        Cursor rs = mydb.getLatestOperations();
        rs.moveToFirst();
        for ( int i = 0 ; i < rs.getCount(); i ++ ) {
            list1.add(rs.getString(rs.getColumnIndex(DB.LO_COLUMN_CATEGORY)) + " || "  + rs.getString(rs.getColumnIndex(DB.LO_COLUMN_AMOUNT)));
            Log.i("info", rs.getString(rs.getColumnIndex(DB.LO_COLUMN_CATEGORY)) + " || "  + rs.getString(rs.getColumnIndex(DB.LO_COLUMN_AMOUNT)));
            if (i < rs.getCount()-1) rs.moveToNext();
        }
        if(!rs.isClosed()){
            rs.close();
        }
//
//
//        list1.addAll(Arrays.asList(rs));
        adapter = new ArrayAdapter<String>(this,R.layout.list_element, list1);
        list.setAdapter(adapter);

        }
    }

