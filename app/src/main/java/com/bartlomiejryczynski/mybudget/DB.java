package com.bartlomiejryczynski.mybudget;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DB extends SQLiteOpenHelper{


    public static final String DATABASE_NAME = "MyBudget.db";
    public static final String LO_TABLE_NAME = "lo";
    public static final String LO_COLUMN_ID = "id";
    public static final String LO_COLUMN_CATEGORY = "category";
    public static final String LO_COLUMN_AMOUNT = "amount";
    public static final String LO_COLUMN_ACCOUNTID = "accountid";
    public static final String LO_COLUMN_USERACCOUNT = "useraccount";
    public static final String BA_COLUMN_ID = "id";
    public static final String BA_COLUMN_name="name";
    public static final String BALANCE_COLUMN_ID="id";
    public static final String BALANCE_COLUMN_AMOUNT = "amount";
    public static final String BALANCE_COLUMN_ACCOUNTID="accountid";


    public DB(Context context) {
        super(context, DATABASE_NAME , null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table lo " +
                        "(id integer primary key, category text,amount double, accountid text,useraccount text)"
        );
        db.execSQL("create table categories ( id integer primary key, category text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS lo");
        db.execSQL("DROP TABLE IF EXISTS categories");
        onCreate(db);
    }

    public boolean insertOperation (String category, Double amount, String accountid ,String  useraccount ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("category", category);
        contentValues.put("amount", amount);
        contentValues.put("accountid", accountid);
        contentValues.put("useraccount", useraccount);

        db.insert("lo", null, contentValues);
        return true;
    }

    public Cursor getLatestOperations(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from lo order by id desc LIMIT 10", null);
        return res;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from lo where id="+id+"", null );
        return res;
    }

    public Cursor getCategoryNames(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select category from categories", null);
        return res;
    }

    public boolean InsertCategory(String categoryname){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("category", categoryname );

        db.insert("categories", null , contentValues);
        return true;
    }
}

